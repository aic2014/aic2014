import twitter4j.Status;


public class ClassifiedTweetWithStatus {
	
	private Double sentiment;
	private Status tweet;
	private Double weight;
	
	public ClassifiedTweetWithStatus(Double sentiment, Status tweet){
		this.sentiment = sentiment;
		this.tweet = tweet;
	}
	
	public Double getSentiment() {
		return sentiment;
	}
	public void setSentiment(Double sentiment) {
		this.sentiment = sentiment;
	}
	public Status getTweet() {
		return tweet;
	}
	public void setTweet(Status tweet) {
		this.tweet = tweet;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}
	
}


public class ClassifiedTweet {
	private String tweet;
	private Double sentiment;
	private Double weigth;
	
	public Double getWeigth() {
		return weigth;
	}

	public void setWeigth(Double weigth) {
		this.weigth = weigth;
	}

	public ClassifiedTweet(String tweet,Double sentiment){
		this.tweet = tweet;
		this.sentiment = sentiment;
	}

	public String getTweet() {
		return tweet;
	}

	public Double getSentiment() {
		return sentiment;
	}

	public void setTweet(String tweet) {
		this.tweet = tweet;
	}

	public void setSentiment(Double sentiment) {
		this.sentiment = sentiment;
	}
	
}

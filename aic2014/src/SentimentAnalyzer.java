import java.io.IOException;
import java.util.ArrayList;
 
public class SentimentAnalyzer {
 
    public static void main(String[] args) {
        String topic = "#nasa";
        TweetManager tweetManager;
        NLP nlp;
        ArrayList<String> tweets;
		try {
			tweetManager = new TweetManager();
			nlp = new NLP();
	        tweets = tweetManager.getTweets(topic, 1);
	        for(String tweet : tweets) {
	            System.out.println(tweet + " : " + nlp.findSentiment(tweet));
	        }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}

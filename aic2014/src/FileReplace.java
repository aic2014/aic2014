import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

class FileReplace
{
    ArrayList<String> lines = new ArrayList<String>();
    String line = null;
    public void  doIt()
    {
        try
        {
            File f1 = new File("files/secondrow.csv");
            FileReader fr = new FileReader(f1);
            BufferedReader br = new BufferedReader(fr);
            while ((line = br.readLine()) != null)
            {
               // if (!line.startsWith("\"")&&!line.isEmpty())
                //    line = "\""+line+"\n";
              //  else 
              //  	line = line + "\n";
              //  System.out.println(line);
            	line = line.replaceAll("\"", "");
            	 line = "\""+line+"\"\n";
            	//System.out.println(line);
                lines.add(line);
            }
            fr.close();
            br.close();

            FileWriter fw = new FileWriter(f1);
            BufferedWriter out = new BufferedWriter(fw);
            for(String s : lines)
                 out.write(s);
            out.flush();
            out.close();
            System.out.println("Done");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public static void main(String args[])
    {
        FileReplace fr = new FileReplace();
        fr.doIt();
    }
}
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
 
public class TweetManager {
	private HashMap<String, String> stopWords;
	private HashMap<String, String> emoticons;
	private HashMap<String, String> acronyms;
	
	public TweetManager() throws IOException {
		/* load stop words */
		stopWords = new HashMap<String, String>();
		BufferedReader in = new BufferedReader(new FileReader("files/stopwords.txt"));
        String line = "";
        while ((line = in.readLine()) != null) {
        	stopWords.put(line, line);
        }
        in.close();
        
        /**
         * load emoticon mapping (from wikipedia emoticon list)
         * each line contains an emoticon and it's mapping seperated by a tabulator
         * */
        emoticons = new HashMap<String, String>();
        in = new BufferedReader(new FileReader("files/emoticons.txt"));
        line = "";
        String[] parts;
        while ((line = in.readLine()) != null) {
        	parts = line.split("\\t");
        	emoticons.put(parts[0], parts[1]);
        }
        in.close();
        
        /**
         * load acronym mapping (dictionary from http://www.noslang.com/)
         * each line contains an acronym and it's mapping seperated by a tabulator
         * */
        acronyms = new HashMap<String, String>();
        in = new BufferedReader(new FileReader("files/acronyms.txt"));
        line = "";
        while ((line = in.readLine()) != null) {
        	parts = line.split("\\t");
        	acronyms.put(parts[0], parts[1]);
        }
        in.close();
	}
	
	public ArrayList<String> getTweets(String topic, int pages) {
		Twitter twitter = new TwitterFactory().getInstance();
        ArrayList<String> tweetList = new ArrayList<String>();
        try {
            Query query = new Query(topic);
            query.count(100);
            query.lang("en");
            QueryResult result;
            int pagesFetched = 0;
            do {
                result = twitter.search(query);
                List<Status> tweets = result.getTweets();
                for (Status tweet : tweets) {
                    tweetList.add(processTweet(tweet.getText()));
                }
                pagesFetched++;
            } while ((pages == 0 || pages > pagesFetched) && (query = result.nextQuery()) != null);
        } catch (TwitterException te) {
            te.printStackTrace();
            System.out.println("Failed to search tweets: " + te.getMessage());
        }
        return tweetList;
	}
 
	//duplicate of getTweets, just returns Statuses for later aggregation 
	public ArrayList<Status> getTweetsWithStatus(String topic, int pages) {
		Twitter twitter = new TwitterFactory().getInstance();
        ArrayList<Status> tweetList = new ArrayList<>();
        try {
            Query query = new Query(topic);
            query.count(100);
            query.lang("en");
            QueryResult result;
            int pagesFetched = 0;
            do {
                result = twitter.search(query);
                List<Status> tweets = result.getTweets();
                for (Status tweet : tweets) {
                    tweetList.add(tweet);
                }
                pagesFetched++;
            } while ((pages == 0 || pages > pagesFetched) && (query = result.nextQuery()) != null);
        } catch (TwitterException te) {
            te.printStackTrace();
            System.out.println("Failed to search tweets: " + te.getMessage());
        }
        return tweetList;
	}
	
    public ArrayList<String> getTweets(String topic) {
    	return getTweets(topic, 0);
    }
    
    public String processTweet(String tweet) {
    	// replace emoticons
    	tweet = replaceEmoticons(tweet);
    	
    	// convert to lower case
    	tweet = tweet.toLowerCase();

    	// replace acronyms
    	tweet = replaceAcronyms(tweet);
    	
    	// remove urls
    	tweet = removeUrls(tweet);
    	
    	// remove @Username
    	tweet = removeUsername(tweet);
    	
    	// remove hashtag
    	tweet = tweet.replaceAll("#", "");
    	
    	// replace repeated characters
    	tweet = tweet.replaceAll("([a-z])\\1+", "$1$1");
    	
    	// remove additional white space
    	tweet = tweet.replaceAll("\\s+", " ");
    	tweet = tweet.trim();
    	
    	// remove stop words
    	tweet = removeStopWords(tweet);
    	
    	// remove punctuation and special characters
    	tweet = tweet.replaceAll("[^\\w\\s]","");
    	
    	return tweet;
    }
    
    private String removeUrls(String tweet) {
    	String urlPattern = "\\b(?:(?:https?|ftp|file)://|www\\.|ftp\\.)[-A-Z0-9+&@#/%=~_|$?!:,.]*[A-Z0-9+&@#/%=~_|$]";
        Pattern p = Pattern.compile(urlPattern,Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(tweet);
        
        while (m.find()) {
        	tweet = tweet.replaceAll(m.group(),"").trim();
        }
        
        return tweet;
    }
    
    private String removeUsername(String tweet) {
    	String urlPattern = "@\\w+";
        Pattern p = Pattern.compile(urlPattern,Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(tweet);
        
        while (m.find()) {
        	tweet = tweet.replaceAll(m.group(),"").trim();
        }
        
        return tweet;
    }
    
    private String removeStopWords(String tweet) {
    	String[] words = tweet.split("\\s");
    	String result = "";
    	
    	for (String word : words) {
    		if (!stopWords.containsKey(word)) {
    			result += word + " ";
    		}
    	}
    	
    	return result;
    }
    
    private String replaceEmoticons(String tweet) {
    	String[] words = tweet.split("\\s");
    	String result = "";
    	
    	for (String word : words) {
    		if (emoticons.containsKey(word)) {
    			result += emoticons.get(word) + " ";
    		} else {
    			result += word + " ";
    		}
    	}
    	
    	return result;
    }
    
    private String replaceAcronyms(String tweet) {
    	String[] words = tweet.split("\\s");
    	String result = "";
    	
    	for (String word : words) {
    		if (acronyms.containsKey(word)) {
    			result += acronyms.get(word) + " ";
    		} else {
    			result += word + " ";
    		}
    	}
    	
    	return result;
    }
}